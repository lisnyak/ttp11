var gulp = require('gulp');
var sourceFiles = ['src/*'];
var destination = 'dist/';

function copy() {
    return gulp.src(sourceFiles)
        .pipe(gulp.dest(destination));
}
gulp.task("default", copy);
gulp.task("watch", function(){
    return gulp.watch(['src/*.*'], copy);
})